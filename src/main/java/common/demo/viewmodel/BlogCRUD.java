package common.demo.viewmodel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

import common.demo.dao.UserDAO;
import common.demo.model.Blog;
import common.demo.model.User;

public class BlogCRUD {
	private Blog selectedBlog;
	private String recordBlog;
	private List<User> allUser;
	
	@Wire("#blogcrud")
	private Window win;

	public Blog getSelectedBlog() {
		return selectedBlog;
	}

	public void setSelectedBlog(Blog selectedBlog) {
		this.selectedBlog = selectedBlog;
	}

	public String getRecordBlog() {
		return recordBlog;
	}

	public void setRecordBlog(String recordBlog) {
		this.recordBlog = recordBlog;
	}
	
	@Command
	public void closeThis(){
		win.detach();
	}
	
	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("sBlog") Blog c1,
            @ExecutionArgParam("recordBlog") String recordMode) throws CloneNotSupportedException{
		Selectors.wireComponents(view, this, false);
		setRecordBlog(recordMode);
		
		if (recordMode.equals("NEW")){
			this.selectedBlog = new Blog();
		}
		if (recordMode.equals("EDIT")){
			this.selectedBlog = (Blog) c1.clone();
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Command
	public void save(){
		Map args = new HashMap<>();
		args.put("pBlog", this.selectedBlog);
		args.put("recordBlog", this.recordBlog);
		System.out.println("444444444444");
		BindUtils.postGlobalCommand(null, null, "updateBlogList", args);
		win.detach();
	}
	
	public List<User> getAllUser(){
		UserDAO userDao = new UserDAO();
		allUser = userDao.getUsers();
		return allUser;
	}
}
