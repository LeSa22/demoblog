package common.demo.viewmodel;

import java.util.HashMap;
import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Default;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;

import common.demo.dao.BlogDAO;
import common.demo.dao.UserDAO;
import common.demo.model.Blog;
import common.demo.model.User;

public class BlogsService {
	
	BlogDAO blogDAO = new BlogDAO();
	UserDAO userDAO = new UserDAO();
	private Integer curSelectedBlogIndex;
	private List<Blog> blogs = blogDAO.getBlogs();
	private List<User> users;
	public List<Blog> getBlogs() {
		return blogs;
	}
	public List<User> getUsers() {
		return userDAO.getUsers();
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	public Integer getCurSelectedBlogIndex() {
		return curSelectedBlogIndex;
	}
	public void setCurSelectedBlogIndex(Integer curSelectedBlogIndex) {
		this.curSelectedBlogIndex = curSelectedBlogIndex;
	}
	@Command
	public void addUser(){
		final HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("sUser", null);
		map.put("recordMode", "NEW");
		Executions.createComponents("addUser.zul", null, map);
	}
	@Command
	public void addBlog(){
		final HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("sBlog", null);
		map.put("recordBlog", "NEW");
		Executions.createComponents("addBlog.zul", null, map);
	}
	
	@GlobalCommand
	@NotifyChange("users")
	public void updateUserList(@BindingParam("pUser") User us,@BindingParam("recordMode") String recordMode ){
		if(recordMode.equals("NEW")){
			try{
				userDAO.add(us);
				users = userDAO.getUsers();
				Clients.showNotification("Thêm mới thành công!", "info", null, "top_center", 5000);
			}catch(Exception e){
				e.printStackTrace();
				Clients.showNotification("Thêm mới không thành công!", "error", null, "top_center", 5000);
			}
		}
		if(recordMode.equals("EDIT")){
			try{
				userDAO.update(us);
				users = userDAO.getUsers();
				Clients.showNotification("Cập nhập thành công!", "info", null, "top_center", 5000);
			}catch(Exception e){
				e.printStackTrace();
				Clients.showNotification("Cập nhập không thành công!", "error", null, "top_center", 5000);
			}
		}
	}
	
	@GlobalCommand
	@NotifyChange("blogs")
	public void updateBlogList(@BindingParam("pBlog") Blog blog,@BindingParam("recordBlog") String recordMode ){
		if(recordMode.equals("NEW")){
			try{
				blogDAO.add(blog);
				blogs = blogDAO.getBlogs();
				System.out.println("000000");
				Clients.showNotification("Thêm mới thành công!", "info", null, "top_center", 5000);
			}catch(Exception e){
				e.printStackTrace();
				Clients.showNotification("Thêm mới không thành công!", "error", null, "top_center", 5000);
			}
		}
		if(recordMode.equals("EDIT")){
			try{
				blogDAO.update(blog);
				blogs = blogDAO.getBlogs();
				System.out.println("1111111");
				Clients.showNotification("Cập nhập thành công!", "info", null, "top_center", 5000);
			}catch(Exception e){
				e.printStackTrace();
				Clients.showNotification("Cập nhập không thành công!", "error", null, "top_center", 5000);
			}
		}
		System.out.println("Hihi");
	}
	
	@Command
	@NotifyChange("blogs")
	public void getBlogById(@BindingParam("id") int id){
		blogs = blogDAO.getBlogsByUserId(id);
		System.out.println("Hoho");
	}
	
	@Command
	public void edit(@BindingParam("blog") Blog blog){
		if(blog != null){
		final HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("sBlog", blog);
		map.put("recordBlog", "EDIT");
		setCurSelectedBlogIndex(blogs.indexOf(blog));
		Executions.createComponents("addBlog.zul", null, map);
		}
		else{
			System.out.println("ggg");
		}
	}
	
	@Command
	public void delete(@BindingParam("blog") final Blog blog, @Default(value = "*") String attr) {
		Messagebox.show("Bạn muốn xóa mục này?", "Xác nhận", Messagebox.CANCEL | Messagebox.OK,
				Messagebox.QUESTION, new EventListener<Event>() {
					@Override
					public void onEvent(final Event event) {
						if (Messagebox.ON_OK.equals(event.getName())) {
							blogDAO.deleteBlog(blog.getId());
							blogs = blogDAO.getBlogs();
							Clients.showNotification("Xóa thành công!", "info", null, "top_center", 5000);
							BindUtils.postNotifyChange(null, null, BlogsService.this, "*");
						}
					}
				});
	}
}
