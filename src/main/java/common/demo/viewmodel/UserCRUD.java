package common.demo.viewmodel;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

import common.demo.model.User;

public class UserCRUD {

	private User selectedUser;
	private String recordMode;
	
	@Wire("#vbcrud")
	private Window win;

	public User getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
	}

	public String getRecordMode() {
		return recordMode;
	}

	public void setRecordMode(String recordMode) {
		this.recordMode = recordMode;
	}
	
	@Command
	public void closeThis(){
		win.detach();
	}
	
	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("sUser") User c1,
            @ExecutionArgParam("recordMode") String recordMode) throws CloneNotSupportedException{
		Selectors.wireComponents(view, this, false);
		setRecordMode(recordMode);
		
		if (recordMode.equals("NEW")){
			this.selectedUser = new User();
		}
		if (recordMode.equals("EDIT")){
			this.selectedUser = (User) c1.clone();
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Command
	public void save(){
		Map args = new HashMap<>();
		args.put("pUser", this.selectedUser);
		args.put("recordMode", this.recordMode);
		BindUtils.postGlobalCommand(null, null, "updateUserList", args);
		System.out.println("Haha");
		win.detach();
	}
}
