package common.demo.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import common.demo.model.User;
import common.demo.util.HibernateConnector;

public class UserDAO {
	Session session = null;
	public static List<User> users = new ArrayList<User>();
	@SuppressWarnings("unchecked")
	public List<User> getUsers() {
		users = new ArrayList<>();
		session = HibernateConnector.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			Query query = session.createQuery("from User");
			users = query.list();
			System.out.println(users.size());
			if (users == null)
				return null;

			else
				return users;
		} catch (HibernateException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public User add(User us) {
		Transaction transaction = null;
		try {
			session = HibernateConnector.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			session.save(us);
			transaction.commit();
			return us;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public void update(User us) {
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateConnector.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			session.saveOrUpdate(us);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
}
