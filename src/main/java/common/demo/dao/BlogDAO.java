package common.demo.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import common.demo.model.Blog;
import common.demo.util.HibernateConnector;

public class BlogDAO {
	Session session = null;
	public static List<Blog> blogs = new ArrayList<Blog>();
	@SuppressWarnings("unchecked")
	public List<Blog> getBlogs() {
		blogs = new ArrayList<>();
		session = HibernateConnector.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			Query query = session.createQuery("from Blog");
			blogs = query.list();
			if (blogs == null)
				return null;

			else
				return blogs;
		} catch (HibernateException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Blog> getBlogsByUserId(int id) {
		blogs = new ArrayList<>();
		session = HibernateConnector.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			Query query = session.createQuery("from Blog where user_id="+id);
			blogs = query.list();
			if (blogs == null)
				return null;

			else
				return blogs;
		} catch (HibernateException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Blog add(Blog blog) {
		Transaction transaction = null;
		try {
			session = HibernateConnector.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			session.save(blog);
			transaction.commit();
			System.out.println("save");
			return blog;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public void update(Blog blog) {
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateConnector.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			session.saveOrUpdate(blog);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void deleteBlog(int id) {
		Session session = null;
		try {
			session = HibernateConnector.getSessionFactory().openSession();
			Transaction beginTransaction = session.beginTransaction();
			Query query = session.createQuery("delete from Blog v where v.id= :id");
			query.setParameter("id", id);
			query.executeUpdate();
			beginTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
}
