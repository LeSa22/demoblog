package common.demo.voc;

import org.hibernate.Session;

import common.demo.model.Blog;
import common.demo.model.User;
import common.demo.util.HibernateConnector;

/**
 * Hello world!
 *
 */
public class App 
{
	 public static void main( String[] args )
	    {
	    	Session session = HibernateConnector.getSessionFactory().openSession();
	    	session.beginTransaction();
	    	Blog blog = new Blog();
	    	User user = new User();
	    	session.close();
	    }
}
