-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.17-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for voc
CREATE DATABASE IF NOT EXISTS `voc` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_vietnamese_ci */;
USE `voc`;

-- Dumping structure for table voc.blog
CREATE TABLE IF NOT EXISTS `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `blogType` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `dateCreate` date DEFAULT NULL,
  `dateExp` date DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2E2FA267039487` (`user_id`),
  CONSTRAINT `FK2E2FA267039487` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

-- Dumping data for table voc.blog: ~0 rows (approximately)
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
INSERT INTO `blog` (`id`, `address`, `blogType`, `dateCreate`, `dateExp`, `title`, `user_id`) VALUES
	(30, 'http://tienganhb1.com/chung-chi-b2', 'Tiếng Anh', '2017-05-28', '2017-06-08', 'Chứng chỉ B2', 4),
	(31, 'http://tienganhb1.com/chung-chi-b2', 'Learn', NULL, NULL, 'Tin học văn ', 5),
	(33, 'http://www.blognhadat.vn/', 'Nhà đất', '2017-06-07', '2017-06-09', 'Blog nhà đất', 8);
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;

-- Dumping structure for table voc.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `birthday` date DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

-- Dumping data for table voc.user: ~2 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `birthday`, `name`, `password`, `username`) VALUES
	(4, '1996-06-07', 'Jame Bond', '123456', 'Jame@gmail.com'),
	(5, '1867-06-07', 'Aquria', '123456', 'lackKissDy'),
	(6, '1984-07-25', 'Jany Mamy', '123456', 'mamy@gmail.com'),
	(7, '2007-02-09', 'Sơn Bá', '56782390', 'sontran@gmail.com'),
	(8, '2009-07-08', 'Trần Thị Lê Sa', '123456', 'Lesa@gmail.com');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
